### CONFIGMAPS ###

 ConfigMaps é utilizado para passar variáveis em uma aplicação. 
 
 Sua utilização é bastante útil, já que uma vez que o tipo configmaps esteja com as variáveis necessárias, posso utilizar este configMap para várias aplicações sem precisar colocar em cada uma das aplicações as variáveis.
